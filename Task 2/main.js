/*
    Задание 2.

    Сложить в элементе с id App следующую размету HTML:

    <header>
      <a href="http://google.com.ua">
        <img src="https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png">
      </a>
      <div class="menu">
        <a href="#1"> link 1</a>
        <a href="#1"> link 2</a>
        <a href="#1"> link 3</a>
      </div>
    </header>


    Используя следующие методы для работы:
    getElementById
    createElement
    element.innerText
    element.className
    element.setAttribute
    element.appendChild

*/


var header = document.createElement('header');
var a = document.createElement('a');
var div = document.createElement('div');
var img = document.createElement('img');
var a_link1 = document.createElement('a');
var a_link2 = document.createElement('a');
var a_link3 = document.createElement('a');

document.body.appendChild(header);
	header.id = "header";
	header.style.padding = "20px";

document.getElementById('header').appendChild(a);
	a.id = "a";
	a.href = "http://google.com.ua";

document.getElementById('a').appendChild(img);
	img.src = "https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";
	img.setAttribute('alt', 'google_pic');
	img.setAttribute('title', 'Google');

document.getElementById('header').appendChild(div);
	div.id = "div";
	div.className = "menu";
	
document.getElementById('div').appendChild(a_link1);
	a_link1.innerText = " link 1";
	a_link1.href = "#1";
	a_link1.id = "a_menu";
document.getElementById('div').appendChild(a_link2);
	a_link2.innerText = " link 2";
	a_link2.href = "#1";
	a_link2.id = "a_menu";
document.getElementById('div').appendChild(a_link3);
	a_link3.innerText = " link 3";
	a_link3.href = "#1";
	a_link3.id = "a_menu";








	
